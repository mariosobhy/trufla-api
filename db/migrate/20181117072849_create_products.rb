class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :promotion_code
      t.float :discount,default: 0.0
      t.float :price, default: 0.0
      t.boolean :active, default: true
      t.references :department, index: true

      t.timestamps
    end
    add_foreign_key :products, :department
  end
end
