import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr(),
    price: DS.attr(),
    discount: DS.attr(),
    promotion_code: DS.attr(),
    active: DS.attr(),
    department: DS.belongsTo('department')
});
