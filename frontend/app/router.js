import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('departments');
  this.route('departments.new', { path: 'departments/new' });
  this.route('departments', { path: 'departments/:department_id' }, function(){
    this.route('product.new', { path: 'products/new' } )
  });
  this.route('products');
});

export default Router;
