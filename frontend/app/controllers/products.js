import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['promotion_code','department','search'],
    promotion_code: null,
    department: null,
    search: null,

    filteredProducts: function() {
        var department = this.get('department');
        var products = this.get('model');

        if(department) {
            return products.filterBy('department',department);
        }
        else{
            return products;
        }
    }.property('department','model')
});
