class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :discount, :active, :promotion_code
end
