class Api::V1::DepartmentsController < Api::V1::BaseController
  before_action :set_department, only: [:show, :update, :destroy]

  # GET /departments
  def index
    @departments = Department.all
    respond_with @departments
  end

  # GET /departments/1
  def show
    respond_with @department
  end

  # POST /departments
  def create
    @department = Department.new(department_params)

    if @department.save
      #render json: @department, status: :created, location: @department
      respond_with :api, :v1, @department
    else
      #render json: @department.errors, status: :unprocessable_entity
      respond_with :api, :v1, @department.errors
    end
  end

  # PATCH/PUT /departments/1
  def update
    if @department.update(department_params)
      #render json: @department
      respond_with :api, :v1, @department
    else
      #render json: @department.errors, status: :unprocessable_entity
      respond_with :api, :v1, @department.errors
    end
  end

  # DELETE /departments/1
  def destroy
    respond_with @department.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def department_params
      params.require(:department).permit(:name)
    end
end
