class Product < ApplicationRecord
  scope :department, -> (department_id) { where department_id: department_id }
  scope :promotion_code, -> (code) { where promotion_code: code }
  scope :search, -> (search) { where("name LIKE ?", "%#{search}%") }
  belongs_to :department
end
